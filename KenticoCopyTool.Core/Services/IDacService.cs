﻿using ProdKenticoCopyTool.Core.Models;

namespace ProdKenticoCopyTool.Core.Services
{
    public interface IDacService
    {
        void ExportAndRestore(DatabaseInfo source, DatabaseInfo destination);
    }
}

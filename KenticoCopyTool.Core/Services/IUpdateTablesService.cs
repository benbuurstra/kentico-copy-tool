﻿namespace ProdKenticoCopyTool.Core.Services
{
    public interface IUpdateTablesService
    {
        void RunSqlScript(string connectionString, string databaseName, string sqlScriptFileName = "Prod DB Script.sql");
    }
}

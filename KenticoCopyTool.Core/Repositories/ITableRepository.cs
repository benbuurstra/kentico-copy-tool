﻿namespace ProdKenticoCopyTool.Core.Repositories
{
    public interface ITableRepository
    {
        void RunSqlScript(string connectionString, string script);
    }
}

﻿namespace ProdKenticoCopyTool.Core.Repositories
{
    public interface IDacRepository
    {
        void Export(string connectionString, string databaseName);
        void Import(string connectionString, string databaseName);
    }
}

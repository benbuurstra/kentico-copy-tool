﻿namespace ProdKenticoCopyTool.Core.Models
{
    public class DatabaseInfo
    {
        public string ConnectionString { get; set; }
        public string Name { get; set; }
    }
}

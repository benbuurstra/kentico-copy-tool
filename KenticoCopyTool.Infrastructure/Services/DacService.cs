﻿using ProdKenticoCopyTool.Core.Models;
using ProdKenticoCopyTool.Core.Repositories;
using ProdKenticoCopyTool.Core.Services;

namespace ProdKenticoCopyTool.Infrastructure.Services
{
    public class DacService : IDacService
    {
        private readonly IDacRepository dacRepository;

        public DacService(IDacRepository dacRepository)
        {
            this.dacRepository = dacRepository;
        }

        public void ExportAndRestore(DatabaseInfo source, DatabaseInfo destination)
        {
            this.dacRepository.Export(source.ConnectionString, source.Name);
            this.dacRepository.Import(destination.ConnectionString, destination.Name);
        }
    }
}

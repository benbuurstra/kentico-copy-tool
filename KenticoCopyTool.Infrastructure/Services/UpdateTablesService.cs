﻿using System.IO;
using ProdKenticoCopyTool.Core.Repositories;
using ProdKenticoCopyTool.Core.Services;

namespace ProdKenticoCopyTool.Infrastructure.Services
{
    public class UpdateTablesService : IUpdateTablesService
    {
        private readonly ITableRepository tableRepository;

        public UpdateTablesService(ITableRepository tableRepository)
        {
            this.tableRepository = tableRepository;
        }

        public void RunSqlScript(string connectionString, string databaseName, string sqlScriptFileName = "Prod DB Script.sql")
        {
            var script = new FileInfo(sqlScriptFileName)?.OpenText()?.ReadToEnd();

            if (!connectionString.EndsWith(";"))
            {
                connectionString += ";";
            }
            connectionString += $"Initial Catalog={databaseName};";

            this.tableRepository.RunSqlScript(connectionString, script);
        }
    }
}

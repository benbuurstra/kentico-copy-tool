﻿using System;
using System.Windows;
using LightInject;
using ProdKenticoCopyTool.Core.Repositories;
using ProdKenticoCopyTool.Core.Services;
using ProdKenticoCopyTool.Data.Repositories;
using ProdKenticoCopyTool.Infrastructure.Services;

namespace ProdKenticoCopyTool
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            var container = RegisterIoC();

            RunApplication(container);
        }

        private static ServiceContainer RegisterIoC()
        {
            var container = new ServiceContainer();

            container.Register<MainWindow>();

            container.Register<IDacService, DacService>();
            container.Register<IDacRepository, DacRepository>();
            container.Register<IUpdateTablesService, UpdateTablesService>();
            container.Register<ITableRepository, TableRepository>();

            return container;
        }

        private static void RunApplication(ServiceContainer container)
        {
            try
            {
                var app = new App();
                var mainWindow = container.GetInstance<MainWindow>();
                app.Run(mainWindow);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using System.Windows;
using ProdKenticoCopyTool.Core.Models;
using ProdKenticoCopyTool.Core.Services;

namespace ProdKenticoCopyTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IDacService dacService;
        private readonly IUpdateTablesService updateTablesService;

        public MainWindow(
            IDacService dacService,
            IUpdateTablesService updateTablesService)
        {
            this.dacService = dacService;
            this.updateTablesService = updateTablesService;

            this.InitializeComponent();
        }

        private async void Btn_Transfer_Click(object sender, RoutedEventArgs e)
        {
            // Verify text is is all text boxes
            if (string.IsNullOrEmpty(this.TxtDestinationName.Text) ||
                string.IsNullOrEmpty(this.TxtDestinationConnectionString.Text) ||
                string.IsNullOrEmpty(this.TxtSourceName.Text) ||
                string.IsNullOrEmpty(this.TxtSourceConnectionString.Text))
            {
                MessageBox.Show("All fields are required", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Create objects with text box data
            var sourceDatabaseInfo = new DatabaseInfo
            {
                ConnectionString = this.TxtSourceConnectionString.Text,
                Name = this.TxtSourceName.Text
            };

            var destinationDatabaseInfo = new DatabaseInfo
            {
                ConnectionString = this.TxtDestinationConnectionString.Text,
                Name = this.TxtDestinationName.Text
            };

            try
            {
                this.ShowProgressBar();

                await Task.Run(() =>
                {
                    this.dacService.ExportAndRestore(sourceDatabaseInfo, destinationDatabaseInfo);
                    this.updateTablesService.RunSqlScript(destinationDatabaseInfo.ConnectionString, destinationDatabaseInfo.Name);
                });

                this.HideProgressBar();

                MessageBox.Show($"Database {destinationDatabaseInfo.Name} successfully added", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                this.HideProgressBar();

                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }

                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ShowProgressBar()
        {
            this.ProgressBar.Visibility = Visibility.Visible;
        }

        private void HideProgressBar()
        {
            this.ProgressBar.Visibility = Visibility.Hidden;
        }
    }
}

﻿using System.Data.SqlClient;
using Dapper;
using ProdKenticoCopyTool.Core.Repositories;

namespace ProdKenticoCopyTool.Data.Repositories
{
    public class TableRepository : ITableRepository
    {
        public void RunSqlScript(string connectionString, string script)
        {
            using (var db = new SqlConnection(connectionString))
            {
                db.Execute(script);
            }
        }
    }
}

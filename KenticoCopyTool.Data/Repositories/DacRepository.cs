﻿using Microsoft.SqlServer.Dac;
using ProdKenticoCopyTool.Core.Repositories;

namespace ProdKenticoCopyTool.Data.Repositories
{
    public class DacRepository : IDacRepository
    {
        private readonly string fileName = "temp.bacpac";

        public void Export(string connectionString, string databaseName)
        {
            var dacServices = new DacServices(connectionString);

            dacServices.ExportBacpac(this.fileName, databaseName);
        }

        public void Import(string connectionString, string databaseName)
        {
            var dacServices = new DacServices(connectionString);
            var bacPac = BacPackage.Load(this.fileName);

            dacServices.ImportBacpac(bacPac, databaseName);
        }
    }
}
